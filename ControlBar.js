import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';

export default class ControlBar extends React.Component {
    render() {
        return (
            <View style={styles.ControlBar}>
                <TouchableOpacity style={styles.Button} onPress={this.props.changeCamera}>
                    <Text style={styles.Text}>Switch Cameras</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.Button} onPress={this.props.capturePhoto}>
                    <Text style={styles.Text}>Capture Photo</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.Button} onPress={this.props.toggleFilter}>
                    <Text style={styles.Text}>Toggle Filter</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    Button: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ControlBar: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        backgroundColor: '#151515',
        alignItems: 'center',
        justifyContent: 'center'
    },
    Text: {
        color: '#EFEFEF'
    }
});