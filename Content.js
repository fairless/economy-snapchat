import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Camera, Permissions } from 'expo';

export default class Content extends React.Component {
    state = {
        hasCameraPermission: null
    };
  
    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }
  
    render() {
        const { hasCameraPermission } = this.state;
        
        if ((hasCameraPermission === null) || (hasCameraPermission === false)) {
            return (
                <View style={styles.Content}>
                    <Text style={styles.Text}>The application does not have access to the camera.</Text>
                </View>
            );
        } else {
            return (
                <View style={styles.Content}>
                    <Camera style={{flex: 1}} type={this.props.type} whiteBalance={this.props.whiteBalance} ratio={'16:9'} ref={ref => { this.props.setCameraRef(ref); }}>
                    </Camera>
                </View>
            );
        }
    }
}


const styles = StyleSheet.create({
    Content: {
        flex: 8,
        width: '100%'
    },
    Text: {
        color: '#EFEFEF'
    }
});