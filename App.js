import React from 'react';
import { StatusBar, StyleSheet, Text, View, Alert } from 'react-native';
import { Camera } from 'expo';

import TopBar from './TopBar.js';
import Content from './Content.js';
import ControlBar from './ControlBar.js';

export default class App extends React.Component {   
    state = {
        type: Camera.Constants.Type.back,
        whiteBalance: 'fluorescent',
        cameraRef: null
    };
    
    constructor() {
        super();
        
        this.setRef = this.setRef.bind(this);
        
        this.changeCamera = this.changeCamera.bind(this);
        this.toggleFilter = this.toggleFilter.bind(this);
        this.capturePhoto = this.capturePhoto.bind(this);
    }
    
    changeCamera() {
        if (this.state.type === Camera.Constants.Type.back) {
            this.setState({type: Camera.Constants.Type.front});
        } else {
            this.setState({type: Camera.Constants.Type.back});
        }
    }
    
    toggleFilter() {
        if (this.state.whiteBalance === 'sunny') {
            this.setState({whiteBalance: 'fluorescent'});
        } else {
            this.setState({whiteBalance: 'sunny'});
        }
    }
    
    setRef(ref) {
        if (!(this.state.cameraRef)) { 
            this.setState({cameraRef: ref});
        }
    }
    
    capturePhoto() {
        if (this.state.cameraRef) {
            photo = (this.state.cameraRef).takePictureAsync().then(res => {
                console.log(res.uri); // This prints the URI in which the image is saved, but I am unable to find it on my system. A new camera API might be needed.
                Alert.alert(res.uri); // Display URI provided.
            });
        } else {
            this.changeCamera();
        }
    }
    
    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden />
                <TopBar title={"Economy Snapchat"} />
                <Content type={this.state.type} whiteBalance={this.state.whiteBalance} setCameraRef={this.setRef} />
                <ControlBar changeCamera={this.changeCamera} toggleFilter={this.toggleFilter} capturePhoto={this.capturePhoto} />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#202020',
        alignItems: 'center',
        justifyContent: 'center'
    }
});