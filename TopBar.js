import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class TopBar extends React.Component {
    render() {
        return (
            <View style={styles.TopBar}>
                <Text style={styles.Text}>{this.props.title}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    TopBar: {
        flex: 1,
        width: '100%',
        backgroundColor: '#151515',
        alignItems: 'center',
        justifyContent: 'center'
    },
    Text: {
        color: '#EFEFEF'
    }
});